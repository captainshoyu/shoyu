<?php

namespace Shoyu;

class Util
{
    /**
      * Genera slugs a partir de un string dado.
      *
      * @param string $str
      * @param string $separator
      * @return string
      */
    public static function slugify($str, $separator = '-')
    {
        $ascii = self::toAscii(trim($str));
        $cleaned = preg_replace('/[^a-zA-Z0-9\/_|\.+ -]/', '', $ascii);
        $cleaned = strtolower($cleaned);
        $cleaned = preg_replace('/[\/_|\.+ -]+/', $separator, $cleaned);

        return trim($cleaned, $separator);
    }

    /**
     * Reemplaza un caracter no-ascii por uno ascii
     *
     * @param string $char
     * @return string
     */
    public static function toAscii($char)
    {
        $table = [
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a',
            'Þ'=>'B', 'þ'=>'b', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c', 'Ç'=>'C',
            'ç'=>'c', 'Đ'=>'Dj', 'đ'=>'dj', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ẽ'=>'E',
            'Ë'=>'E', 'è'=>'e', 'é'=>'e', 'ẽ'=>'e', 'ê'=>'e', 'ë'=>'e','G̃'=>'G',
            'g̃'=>'g', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ĩ'=>'I', 'Ï'=>'I', 'ì'=>'i',
            'í'=>'i', 'î'=>'i', 'ĩ'=>'i', 'ï'=>'i', 'Ñ'=>'N', 'ñ'=>'n', 'Ò'=>'O',
            'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'ð'=>'o', 'ò'=>'o',
            'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'Ŕ'=>'R', 'ŕ'=>'r',
            'Š'=>'S', 'š'=>'s', 'ß'=>'ss', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ű'=>'U',
            'Ũ'=>'U', 'Ü'=>'U', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ũ'=>'u', 'Ý'=>'Y',
            'Ỹ'=>'Y', 'ý'=>'y', 'ý'=>'y', 'ÿ'=>'y', 'ỹ'=>'y', 'Ž'=>'Z', 'ž'=>'z'
        ];

        return strtr($char, $table);
    }

    /**
     * Normaliza un string, removiendo caracteres que no son alfanuméricos
     * y manteniendo caracteres de codificación UTF-8 como tildes y la letra ñ.
     *
     * @param string $str
     * @return string
     */
    public static function normalizeQuery($str)
    {
        return preg_replace('/([^a-zA-Z0-9á-źÁ-Ź -])/u', '', $str);
    }

    /**
     * Retorna la fecha y la hora actual.
     *
     * @return string
     */
    public static function datetimeNow()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * Verifica que una fecha dada es válida.
     *
     * @param string $date
     * @param string $format
     * @return boolean
     */
    public static function isValidDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Verifica que un email dado sea válido.
     *
     * @param string $email
     * @return boolean
     */
    public static function isValidEmail($email)
    {
        $regex = '/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i';
        $result = preg_match($regex, $email);

        return (boolean) $result ? true : false;
    }
}