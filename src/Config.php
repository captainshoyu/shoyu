<?php

namespace Shoyu;

class Config
{
    protected static $items = [];

    public static function setItems($items)
    {
        self::$items = $items;
    }

    public static function has($key)
    {
        return array_key_exists($key, self::$items);
    }

    public static function all()
    {
        return self::$items;
    }

    public static function get($key)
    {
        return self::has($key) ? self::$items[$key] : null;
    }

    public static function set($key, $value)
    {
        self::$items[$key] = $value;
    }
}