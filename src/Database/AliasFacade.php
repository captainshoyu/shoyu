<?php

namespace Shoyu\Database;

/**
 * Esta clase permite el acceso a métodos no estáticos estáticamente
 */

class AliasFacade
{
    /**
     * @var Shoyu\Database\Connection
     */
    protected static $connectionInstance;

    /**
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        // Call the non-static method from the class instance
        return call_user_func_array([static::$connectionInstance, $method], $args);
    }

    /**
     * @param Shoyu\Database\Connection $connectionInstance
     */
    public static function setConnectionInstance($queryBuilderInstance)
    {
        static::$connectionInstance = $queryBuilderInstance;
    }
}