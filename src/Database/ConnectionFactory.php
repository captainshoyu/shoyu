<?php

namespace Shoyu\Database;

use Shoyu\Database\AliasFacade;

class ConnectionFactory
{
    /**
     * @var string|null
     */
    protected $connectionClassName = 'Shoyu\\Database\\Connection';

    public function __construct($connectionClassName = null)
    {
        if ($connectionClassName) {
            $this->connectionClassName = $connectionClassName;
        }
    }

    /**
     * Crea una instancia de Connection que establece una conexión con PDO
     * @param array $config
     * @param null|string $alias
     * @return Shoyu\Database\Connection 
     */
    public function make(array $config, $alias = null)
    {
        if (! isset($config['driver'])) {
            throw new \InvalidArgumentException('A driver must be specified.');
        }

        $adapter = $this->createAdapter($config['driver']);
        $connectionInstance = new $this->connectionClassName($config, $adapter);

        if ($alias) {
            $this->createAlias($alias, $connectionInstance);
        }

        return $connectionInstance;
    }

    /**
     * Crea una instancia de Shoyu\Database\Query\Adapters\Adapter
     * @param string $driver
     * @return Shoyu\Database\Query\Adapters\PgsqlAdapter|MysqlAdapter|SqliteAdapter
     */
    public function createAdapter($driver)
    {
        $supportedDrivers = ['pgsql', 'mysql', 'sqlite'];
        $driver = strtolower($driver);

        if (! in_array($driver, $supportedDrivers)) {
            throw new \InvalidArgumentException("Unsupported driver [$driver]");
        }

        $adapterName = ucfirst($driver) . 'Adapter';
        $adapterClass = 'Shoyu\\Database\\Query\\Adapters\\' . $adapterName;

        return new $adapterClass;
    }

    /**
     *
     * @param string $alias
     * @param $connectionInstance
     */
    public function createAlias($alias, $connectionInstance)
    {
        class_alias('Shoyu\\Database\\AliasFacade', $alias);
        AliasFacade::setConnectionInstance($connectionInstance);
    }
}