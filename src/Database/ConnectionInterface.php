<?php

namespace Shoyu\Database;

interface ConnectionInterface
{
    public function connect();

    public function setConfig($config);

    public function prepare($query);

    public function execute($query, array $bindings = []);

    public function rowCount($query, array $bindings = []);

    public function fetch($query, array $bindings = []);

    public function fetchAll($query, array $bindings = []);

    public function fetchColumn($query, array $bindings = []);

    public function table($tables);

    public function raw($value, array $bindings = []);

    public function lastInsertId($sequence = []);

    public function getDriverName();

    public function close();
}