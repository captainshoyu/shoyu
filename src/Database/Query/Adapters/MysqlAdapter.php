<?php

namespace Shoyu\Database\Query\Adapters;

use Shoyu\Database\Query\Builder;

class MysqlAdapter extends Adapter
{
    /**
     * @var string
     */
    protected $sanitizer = '`';

    public function compileUpdate(Builder $query, $values)
    {
        $sql = parent::compileUpdate($query, $values);

        if (isset($query->orders)) {
            $sql .= ' ' . $this->compileOrders($query, $query->orders);
        }

        if ($query->limit) {
            $sql .= ' ' . $this->compileLimit($query->limit);
        }

        return $sql;
    }
}