<?php

namespace Shoyu\Database\Query\Adapters;

use Shoyu\Database\Query\Raw;
use Shoyu\Database\Query\Builder;

class Adapter
{
    /**
     * @var array
     */

    protected $selectComponents = [
        'aggregate',
        'columns',
        'tables',
        'joins',
        'wheres',
        'groups',
        'havings',
        'orders',
        'limit',
        'offset'
    ];

    /**
     * @var string
     */
    protected $sanitizer = '"';

    public function compileSelect(Builder $query)
    {
        $select = 'SELECT ';
        $select .= $query->distinct && ! $query->aggregate ? 'DISTINCT ' : '';

        $originalColumns = $query->columns;

        if (is_null($query->columns) && is_null($query->aggregate)) {
            $query->columns = ['*'];
        }

        $select .= trim($this->concatenate($this->compileComponents($query)));

        $query->columns = $originalColumns;

        return $select;
    }

    public function compileComponents(Builder $query)
    {
        $sqlArray = [];

        foreach ($this->selectComponents as $component) {
            if (! is_null($query->{$component})) {
                $method = 'compile' . ucfirst($component);
                $sqlArray[] = $this->{$method}($query->$component);
            }
        }

        return $sqlArray;
    }

    public function compileAggregate($aggregate)
    {
        list($function, $column, $distinct) = $aggregate;

        $column = $this->wrap($column);

        if ($distinct && $column !== '*') {
            $column = 'DISTINCT ' . $column;
        }

        return "$function($column)";
    }

    public function compileColumns($columns)
    {
        return $this->columnize($columns);
    }

    public function compileTables($tables)
    {
        return 'FROM ' . implode(', ', $this->wrapArray($tables));
    }

    public function compileJoins($joins)
    {
        return implode(' ', $joins);
    }

    public function compileWheres($wheres)
    {
        if (is_null($wheres)) {
            return '';
        }

        return 'WHERE ' . $this->removeLeadingBoolean(implode(' ', $wheres));
    }

    public function compileGroups($groups)
    {
        return  'GROUP BY ' . $this->columnize($groups);
    }

    public function compileHavings($havings)
    {
        return 'HAVING ' . $this->removeLeadingBoolean(implode(' ', $havings));
    }

    public function compileOrders($orders)
    {
        return 'ORDER BY ' . implode(', ', array_map(function($order) {
            return $this->wrap($order[0]) . ' ' . $order[1];
        }, $orders));
    }

    public function compileLimit($limit)
    {
        return 'LIMIT ' . (int) $limit;
    }

    public function compileOffset($offset)
    {
        return 'OFFSET ' . (int) $offset;
    }

    public function compileExists(Builder $query)
    {
        $select = $this->compileSelect($query);

        return "SELECT EXISTS ($select)";
    }

    public function compileInsert(Builder $query, array $values)
    {
        $table = $this->wrap(reset($query->tables));

        if (! is_array(reset($values))) {
            $values = [$values];
        }

        $columns = $this->columnize(array_keys(reset($values)));
        $parameters = [];

        foreach ($values as $record) {
            $parameters[] = '(' . $this->parameterize($record) . ')';
        }

        $parameters = implode(', ', $parameters);

        return "INSERT INTO $table ($columns) VALUES $parameters";
    }

    public function compileInsertGetId(
        Builder $query, $values, $sequence = null
    ) {
        return $this->compileInsert($query, $values);
    }

    public function compileUpdate(Builder $query, $values)
    {
        $table = $this->wrap(reset($query->tables));

        $columns = [];

        foreach ($values as $key => $value) {
            $columns[] = $this->wrap($key) . ' = ' . $this->parameter($value);
        }

        $columns = implode(', ', $columns);

        if (! is_null($query->joins)) {
            $joins = ' ' . $this->compileJoins($query->joins);
        } else {
            $joins = '';
        }

        $where = $this->compileWheres($query->wheres);

        return "UPDATE {$table}{$joins} SET $columns $where";
    }

    public function compileDelete(Builder $query)
    {
        $table = $this->wrap(reset($query->tables));
        $wheres = is_array($query->wheres) ? $this->compileWheres($query->wheres) : '';

        return trim("DELETE FROM $table $wheres");
    }

    public function columnize($columns)
    {
        return implode(', ', $this->wrapArray($columns));
    }

    public function wrapArray(array $arr)
    {
        return array_map([$this, 'wrap'], $arr);
    }

    public function wrap($value)
    {
        if ($value instanceof Raw) {
            return (string) $value;
        }

        if (strpos(strtoupper($value), ' AS ') !== false) {
            $pieces = explode(' ', $value);

            return (
                $this->wrap($pieces[0]) . ' AS ' . $this->wrapValue($pieces[2])
            );
        }

        $pieces = explode('.', $value);

        foreach ($pieces as $index => $value) {
            $pieces[$index] = $this->wrapValue($value);
        }

        return implode('.', $pieces);
    }

    public function wrapValue($value)
    {
        if ($value === '*') return $value;

        $s = $this->sanitizer;

        return $s . str_replace($s, ($s . $s), $value) . $s;
    }

	public function concatenate($elements)
    {
        return implode(' ', array_filter($elements, function($e) {
            return ! empty($e) ?: $e;
        }));
    }

    public function parameterize($values)
    {
        return implode(', ', array_map([$this, 'parameter'], $values));
    }

    public function parameter($value)
    {
        return ($value instanceof Raw) ? (string) $value : '?';
    }

    public function removeLeadingBoolean($value)
    {
    	return preg_replace('/AND |OR /i', '', $value, 1);
    }
}