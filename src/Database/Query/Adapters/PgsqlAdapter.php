<?php

namespace Shoyu\Database\Query\Adapters;

use Shoyu\Database\Query\Builder;

class PgsqlAdapter extends Adapter
{
    public function compileInsertGetId(Builder $query, $values, $sequence = null) {
        if (is_null($sequence)) {
            $sequence = 'id';
        }

        return $this->compileInsert($query, $values)
                . ' RETURNING '
                . $this->wrap($sequence);
    }
}