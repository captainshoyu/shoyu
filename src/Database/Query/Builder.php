<?php

namespace Shoyu\Database\Query;

use Closure;
use InvalidArgumentException;
use Shoyu\Database\ConnectionInterface;
use Shoyu\Database\Query\Adapters\Adapter;

class Builder
{
    /**
     * La instancia de Connection.
     *
     * @var Connection.
     */
    protected $connection;

    /**
     * El adaptador de sentencias de Builder a SQL.
     *
     * @var Adapter.
     */
    protected $adapter;

    /**
     * @var array
     */
    public $aggregate;

    /**
     * @var array
     */
    public $columns;

    /**
     * @var bool
     */
    public $distinct = false;

    /**
     * @var array
     */
    public $tables;

    /**
     * @var array
     */
    public $joins;

    /**
     * @var array.
     */
    public $wheres;

    /**
     * @var array
     */
    public $groups;

    /**
     * @var array
     */
    public $havings;

    /**
     * @var array
     */
    public $orders;

    /**
     * @var int
     */
    public $limit;

    /**
     * @var int
     */
    public $offset;

    /**
     * Los parámetros que serán atadas a una consulta SQL.
     *
     * @var array.
     */
    protected $bindings = [
        'select' => [],
        'from'   => [],
        'join'   => [],
        'where'  => [],
        'having' => []
    ];

    public function __construct(ConnectionInterface $connection, Adapter $adapter)
    {
        // Verificar si la instancia de $connection tiene una conexión activa
        if (is_null($connection->getPdo())) {
            $connection->connect();
        }

        $this->connection = $connection;
        $this->adapter = $adapter;
    }

    public function table($tables)
    {
        $tables = is_array($tables) ? $tables : func_get_args();
        
        $instance = new static($this->connection, $this->adapter);

        $instance->from($tables);

        return $instance;
    }

    public function select($columns = ['*'])
    {
        $columns = is_array($columns) ? $columns : func_get_args();

        return $this->addItems($columns, 'columns', 'select');
    }

    public function selectRaw($rawExpression, array $bindings = [])
    {
        return $this->select($this->raw($rawExpression, $bindings));
    }

    public function distinct()
    {
        $this->distinct = true;

        return $this;
    }

    public function selectDistinct($columns)
    {
        $columns = is_array($columns) ? $columns : func_get_args();

        $this->select($columns);
        $this->distinct();

        return $this;
    }

    public function from($tables)
    {
        $tables = is_array($tables) ? $tables : func_get_args();

        return $this->addItems($tables, 'tables', 'from');
    }

    public function addItems($items, $property, $forBinding)
    {
        foreach ($items as $item) {
            if ($item instanceof Raw) {
                $bindings = $item->getBindings();

                if ($bindings) {
                    $this->addBinding($forBinding, $bindings);
                }
            }

            $this->{$property}[] = $item;
        }

        return $this;
    }

    public function groupBy($columns)
    {
        $columns = is_array($columns) ? $columns : func_get_args();
        $this->groups = array_merge((array) $this->groups, $columns);

        return $this;
    }

    public function orderBy($column, $direction = 'ASC')
    {
        $column = is_array($column) ? $column : [$column];

        foreach ($column as $key => $value) {
            if (is_int($key)) {
                $this->orders[] = [$value, $direction];
            } else {
                $this->orders[] = [$key, $value];
            }
        }

        return $this;
    }

    public function limit($limit)
    {
        if ($limit >= 0) {
            $this->limit = $limit;
        }

        return $this;
    }

    public function take($value)
    {
        return $this->limit($value);
    }

    public function offset($offset)
    {
        $this->offset = max(0, $offset);

        return $this;
    }

    public function skip($value)
    {
        return $this->offset($value);
    }

    public function join(
        $table,
        $key,
        $operator = null,
        $value = null,
        $type = 'INNER',
        $method = 'on'
    ) {
        $join = new JoinClause($this->connection, $this->adapter);
        // $join = & $join;

        if ($key instanceof Closure) {
            call_user_func($key, $join);
        } else {
            $join->{$method}($key, $operator, $value);
        }

        $clauses = implode(' ', $join->wheres);
        $clauses = $this->adapter->removeLeadingBoolean($clauses);
        $table = $this->adapter->wrap($table);

        $this->joins[] = "$type JOIN $table ON $clauses";

        $this->addBinding('join', $join->getBindings());

        return $this;
    }

    public function joinWhere($table, $key, $operator, $value, $type = 'INNER')
    {
        return $this->join($table, $key, $operator, $value, $type, 'where');
    }

    public function leftJoin($table, $key, $operator = null, $value = null)
    {
        return $this->join($table, $key, $operator, $value, 'LEFT');
    }

    public function leftJoinWhere($table, $key, $operator, $value)
    {
        return $this->joinWhere($table, $key, $operator, $value, 'LEFT');
    }

    public function rightJoin($table, $key, $operator = null, $value = null)
    {
        return $this->join($table, $key, $operator, $value, 'RIGHT');
    }

    public function rightJoinWhere($table, $key, $operator, $value)
    {
        return $this->joinWhere($table, $key, $operator, $value, 'RIGHT');
    }

    public function where(
        $column, $operator = null, $value = null, $boolean = 'AND'
    ) {
        if (is_array($column)) {
            return $this->addArrayOfWheres($column, $boolean);
        }

        if (func_num_args() == 2) {
            list($value, $operator) = [$operator, '='];
        }

        if ($column instanceof Closure){
            return $this->whereNested($column, $boolean);
        }

        if (is_null($value)) {
            $value = $this->raw('NULL');
        }

        // Criteria Ej: id = ? o id = (select ... from ...) 
        list($criteria, $bindings) = $this->makeCriteria(
            $column, $operator, $value
        );

        if (! $bindings instanceof Raw) {
            $this->addBinding('where', $bindings);
        }

        $this->wheres[] = "$boolean $criteria";

        return $this;
    }

    public function orWhere($column, $operator = null, $value = null)
    {
        return $this->where($column, $operator, $value, 'OR');
    }

    protected function addArrayOfWheres($column, $boolean, $method = 'where')
    {
        return $this->whereNested(function ($query) use ($column, $method) {
            foreach ($column as $key => $value) {
                if (is_numeric($key) && is_array($value)) {
                    // $query->{$method}(...array_values($value)); // PHP >= 5.6
                    call_user_func_array([$query, $method], $value);
                } else {
                    $query->$method($key, '=', $value);
                }
            }
        }, $boolean);
    }

    public function whereRaw($sql, array $bindings = [], $boolean = 'AND')
    {
        $this->wheres[] = "$boolean $sql";

        if($bindings) {
            $this->addBinding('where', $bindings);
        }
        
        return $this;
    }

    public function orWhereRaw($sql, array $bindings = [])
    {
        return $this->whereRaw($sql, $bindings, 'OR');
    }

    public function whereBetween(
        $column, array $values, $boolean = 'AND', $not = false
    ) {
        $between = $not ? 'NOT BETWEEN' : 'BETWEEN';
        $column = $this->adapter->wrap($column);

        $this->whereRaw("$column $between ? AND ?", $values, $boolean);

        return $this;
    }

    public function orWhereBetween($sql, array $values)
    {
        return $this->whereBetween($sql, $values, 'OR');
    }

    public function whereNotBetween($column, array $values, $boolean = 'AND')
    {
        return $this->whereBetween($column, $values, $boolean, true);
    }

    public function orWhereNotBetween($column, array $values)
    {
        return $this->whereNotBetween($column, $values, 'OR');
    }

    public function whereNested(Closure $callback, $boolean = 'AND')
    {
        $query = $this->newQuery();

        call_user_func($callback, $query);

        $wheres = implode(' ', $query->wheres);
        $wheres = $this->adapter->removeLeadingBoolean($wheres);

        $this->wheres[] = "$boolean ($wheres)";

        $this->addBinding('where', $query->getBindings());

        return $this;
    }

    public function whereIn($column, $values, $boolean = 'AND', $not = false)
    {
        $in = $not ? 'NOT IN' : 'IN';

        if ($values instanceof Raw) {
            $parameters = (string) $values;
            $bindings = $values->getBindings();
        } else {
            $parameters = '(' . $this->adapter->parameterize($values) . ')';
            $bindings = $values;
        }

        $column = $this->adapter->wrap($column);

        return $this->whereRaw("$column $in $parameters", $bindings, $boolean);
    }

    public function orWhereIn($column, $values)
    {
        return $this->whereIn($column, $values, 'OR');
    }

    public function whereNotIn($column, $values, $boolean = 'AND')
    {
        return $this->whereIn($column, $values, $boolean, true);
    }

    public function orWhereNotIn($column, $values)
    {
        return $this->whereNotIn($column, $values, 'OR');
    }

    public function whereNull($column, $boolean = 'AND', $not = false)
    {
        $null = $not ? 'NOT NULL' : 'NULL';
        $column = $this->adapter->wrap($column);

        return $this->whereRaw("$column IS $null", [], $boolean);
    }

    public function orWhereNull($column)
    {
        return $this->whereNull($column, 'OR');
    }

    public function whereNotNull($column, $boolean = 'AND')
    {
        return $this->whereNull($column, $boolean, true);
    }

    public function orWhereNotNull($column)
    {
        return $this->whereNotNull($column, 'OR');
    }

    public function whereNot($column, $operator = null, $value = null)
    {
        return $this->where($column, $operator, $value, 'AND NOT');
    }

    public function orWhereNot($column, $operator = null, $value = null)
    {
        return $this->where($column, $operator, $value, 'OR NOT');
    }

    public function having($column, $operator, $value, $boolean = 'AND')
    {
        list($criteria, $bindings) = $this->makeCriteria(
            $column, $operator, $value
        );

        $this->addBinding('having', $bindings);

        $this->havings[] = "$boolean $criteria";

        return $this;
    }

    public function orHaving($column, $operator, $value)
    {
        return $this->having($column, $operator, $value, 'OR');
    }

    public function havingRaw($sql, array $bindings = [], $boolean = 'AND')
    {
        $this->addBinding('having', $bindings);

        $this->havings[] = "$boolean $sql";

        return $this;
    }

    public function orHavingRaw($sql, array $bindings = [])
    {
        return $this->havingRaw($sql, $bindings, 'OR');
    }

    protected function makeCriteria($key, $operator, $value)
    {
        $key = $this->adapter->wrap($key);

        if ($value instanceof Raw) {
            $bindings = $value->getBindings();
            $value = (string) $value;
        } else {
            $bindings = $value;
            $value = '?';
        }

        return ["$key $operator $value", $bindings];
    }

    public function subQuery(Builder $query, $as = '')
    {
        $value = '(' . $query->toSql() . ')';

        if (! empty($as)) {
            $value .= ' AS ' . $this->adapter->wrap($as);
        }

        return $this->raw($value, $query->getBindings());
    }

    public function raw($value, $bindings = [])
    {
        return new Raw($value, $bindings);
    }

    public function toSql()
    {
        return $this->adapter->compileSelect($this);
    }

    public function find($id, array $columns = ['*'])
    {
        return $this->findBy('id', $id, $columns);
    }

    public function findBy($field, $value, array $columns = ['*'])
    {
        return $this->where($field, '=', $value)->first($columns);
    }

    public function first(array $columns = ['*'])
    {
        $result = $this->take(1)->get($columns);

        return count($result) > 0 ? reset($result) : null;
    }

    public function get(array $columns = ['*'])
    {
        $original = $this->columns;

        if (is_null($original)) {
            $this->columns = $columns;
        }

        $results = $this->connection->fetchAll(
            $this->toSql(), $this->getBindings()
        );

        $this->columns = $original;

        return $results;
    }

    public function exists()
    {
        $sql = $this->adapter->compileExists($this);

        return (bool) $this->connection->fetchColumn($sql, $this->getBindings());
    }

    public function count($columns = '*')
    {
        return (int) $this->aggregate(__FUNCTION__, $columns);
    }

    public function min($column)
    {
        return $this->aggregate(__FUNCTION__, $column);
    }

    public function max($column)
    {
        return $this->aggregate(__FUNCTION__, $column);
    }

    public function sum($column)
    {
        return $this->aggregate(__FUNCTION__, $column);
    }

    public function avg($column)
    {
        return $this->aggregate(__FUNCTION__, $column);
    }

    public function aggregate($function, $columns = '*')
    {
        $this->aggregate = [$function, $columns, $this->distinct];

        $previousColumns = $this->columns;
        $previousSelectBindings = $this->bindings['select'];
        
        $this->bindings['select'] = [];
        $this->columns = null;

        $result = $this->connection->fetchColumn(
            $this->toSql(), $this->getBindings()
        );

        $this->aggregate = null;
        $this->columns = $previousColumns;
        $this->bindings['select'] = $previousSelectBindings;

        return $result;
    }

    public function insert(array $values)
    {
        if (empty($values)) {
            throw new InvalidArgumentException('Empty array for insert()');
        }

        if (! is_array(reset($values))) {
            $values = [$values];
        }

        // Ordenar el array de acuerdo a sus claves para no tener problemas con
        // la posición de las columnas.
        else {
            foreach ($values as $key => $value) {
                ksort($value);
                $values[$key] = $value;
            }
        }

        $bindings = [];

        foreach ($values as $record) {
            foreach ($record as $value) {
                $bindings[] = $value;
            }
        }

        $sql = $this->adapter->compileInsert($this, $values);

        return $this->connection->rowCount($sql, $bindings);
    }

    public function insertGetId(array $values, $sequence = null)
    {
        $sql = $this->adapter->compileInsertGetId($this, $values, $sequence);

        $bindings = array_values($values);

        $statement = $this->connection->execute($sql, $bindings);

        if (is_null($sequence)) {
            $id = $this->connection->lastInsertId();
        } else {
            $results = $statement->fetchAll();
            $sequence = $sequence ?: 'id';
            $result = (array) $results[0];
            $id = $result[$sequence];
        }

        return is_numeric($id) ? (int) $id : $id;
    }

    public function update(array $values)
    {
        $bindings = array_values(array_merge($values, $this->getBindings()));

        $sql = $this->adapter->compileUpdate($this, $values);

        return $this->connection->rowCount($sql, $this->cleanBindings($bindings));
    }

    public function increment($column, $amount = 1, array $extra = [])
    {
        if (! is_numeric($amount)) {
            throw new InvalidArgumentException('Non-numeric value passed to increment method.');
        }

        $wrapped = $this->adapter->wrap($column);
        $columns = array_merge([$column => $this->raw("$wrapped + $amount")], $extra);

        return $this->update($columns);
    }

    public function decrement($column, $amount = 1, array $extra = [])
    {
        if (! is_numeric($amount)) {
            throw new InvalidArgumentException('Non-numeric value passed to decrement method.');
        }

        $wrapped = $this->adapter->wrap($column);
        $columns = array_merge([$column => $this->raw("$wrapped - $amount")], $extra);

        return $this->update($columns);
    }

    public function delete($id = null)
    {
        if (! is_null($id) && count($this->tables) == 1) {
            $table = $this->tables[0];
            $this->where($table.'.id', '=', $id);
        }

        return $this->connection->rowCount(
            $this->adapter->compileDelete($this), $this->getBindings()
        );
    }

    public function newQuery()
    {
        return new static($this->connection, $this->adapter);
    }

    public function cleanBindings(array $bindings)
    {
        return array_values(
            array_filter($bindings, function ($binding) {
                return ! $binding instanceof Raw;
            })
        );
    }

    public function addBinding($key, $value)
    {
        if (is_array($value)) {
            $this->bindings[$key] = array_merge($this->bindings[$key], $value);
        } else {
            $this->bindings[$key][] = $value;
        }
    }

    public function getBindings()
    {
        $bindings = [];

        foreach ($this->bindings as $key => $value) {
            if (! empty($value)) {
                $bindings = array_merge($bindings, $value);
            }
        }

        return $bindings;
    }

    public function getRawBindings()
    {
        return $this->bindings;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function getAdapter()
    {
        return $this->adapter;
    }

    public function __toString()
    {
        return (string) $this->toSql();
    }
}