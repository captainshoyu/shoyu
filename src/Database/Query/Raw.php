<?php

namespace Shoyu\Database\Query;

class Raw
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @var array
     */
    protected $bindings;

    public function __construct($value, array $bindings = [])
    {
        $this->value = $value;
        $this->bindings = $bindings;
    }

    public function getBindings()
    {
        return $this->bindings;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        return (string) $this->value;
    }
}