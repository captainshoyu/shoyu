<?php

namespace Shoyu\Database\Query;

class JoinClause extends Builder
{
    public function on($key, $operator, $value, $boolean = 'AND')
    {
        $value = $this->adapter->wrap($value);

        return $this->where($key, $operator, $this->raw($value), $boolean);
    }

    public function orOn($key, $operator, $value)
    {
        return $this->on($key, $operator, $value, 'OR');
    }
}