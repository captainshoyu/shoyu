<?php

namespace Shoyu\Database;

use PDO;
use InvalidArgumentException;
use Shoyu\Database\Query\Adapters\Adapter;
use Shoyu\Database\Query\Builder as QueryBuilder;
use Shoyu\Database\Query\Raw;

/*
 * Clase encargada del manejo de la conexión a la base de datos y de ejecutar
 * las sentencias SQL.
 */

class Connection implements ConnectionInterface
{
    /**
     * La instancia de PDO.
     *
     * @var PDO
     */
    protected $pdo;

    /**
     * Configuración para la conexión a una base de datos.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Opciones por defecto para la conexión a la bse datos.
     *
     * @var array
     */
    protected $options = [
        PDO::ATTR_CASE => PDO::CASE_NATURAL,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_STRINGIFY_FETCHES => false,
        // PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ];

    /**
     * El fetch mode por defecto.
      *
      * @var array
      */
    protected $fetchMode = [PDO::FETCH_ASSOC];

    /**
     * La implementación de un Adapter
     *
     * @var \Shoyu\Database\Query\Adapters\Adapter
     */
    protected $queryAdapter;

    public function __construct(array $config = [], Adapter $queryAdapter = null)
    {
        $this->queryAdapter = $queryAdapter ?: new Adapter;
        $this->config = $config;
    }

    /**
     * Realiza la conexión a la base de datos.
     *
     * @return Connection
     * @throws \PDOException
     */
    public function connect()
    {
        if (is_null($this->pdo)) {
            $config = $this->config;
            $dsn = $this->buildDSN($config);
            $user = isset($config['username']) ? $config['username'] : null;
            $pass = isset($config['password']) ? $config['password'] : null;

            // Si en la configuración existen una o varias opciones establecidas
            // éstas sobreescribirán las opciones por defecto.
            if (array_key_exists('options', $config)) {
                $options = $config['options'] + $options;
            } else {
                $options = $this->options;
            }

            $this->pdo = new PDO($dsn, $user, $pass, $options);
        }

        if (isset($config['charset'])) {
            $charset = $config['charset'];
            $this->execute("SET NAMES '$charset'");
        }

        return $this;
    }

    /**
     * Establece la configuración para la conexión a una BD.
     *
     * @return Connection
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Genera el DSN que será usada para la conexión a una BD.
     *
     * @param array $config.
     * @return string.
     */
    protected function buildDSN($config)
    {
        $driver = $config['driver'];
        $host = isset($config['host']) ? $config['host'] : '';
        $db = $config['database'];
        $port = isset($config['port']) ? $config['port'] : '';
        $dsn = '{driver}:{host}{port}{db}';
        $find = ['/{driver}/', '/{host}/', '/{port}/', '/{db}/'];

        if ($driver == 'sqlite') {
            $host = '';
            $port = '';
        } else {
            $host = ! empty($host) ? "host=$host;" : '';
            $port = ! empty($port) ? "port=$port;" : '';
            $db = ! empty($db) ? "dbname=$db" : '';
        }

        $replaceBy = [$driver, $host, $port, $db];

        return preg_replace($find, $replaceBy, $dsn);
    }

    /**
     * Prepara una setencia SQL.
     *
     * @param string $query.
     * @return PDOStatement.
     */
    public function prepare($query)
    {
        return $this->pdo->prepare($query);
    }

    /**
     * Ejecuta una sentencia preparada.
     *
     * @param string $query.
     * @param array $bindings.
     * @return PDOStatement.
     */
    public function execute($query, array $bindings = [])
    {
        $statement = $this->prepare($query);

        call_user_func_array([$statement, 'setFetchMode'], $this->getFetchMode());

        if ($bindings) {
            // Añadir los valores que serán atados a la sentencia SQL
            foreach ($bindings as $key => $value) {
                $statement->bindValue(
                    is_int($key) ? ($key + 1) : $key,
                    $value,
                    $this->getPdoConstantType($value)
                );
            }
        }

        $statement->execute();

        return $statement;
    }

    public function rowCount($query, array $bindings = [])
    {
        return $this->execute($query, $bindings)->rowCount();
    }

    public function fetch($query, array $bindings = [])
    {
        return $this->execute($query, $bindings)->fetch();
    }

    public function fetchAll($query, array $bindings = [])
    {
        return $this->execute($query, $bindings)->fetchAll();
    }

    public function fetchColumn($query, array $bindings = [])
    {
        return $this->execute($query, $bindings)->fetchColumn();
    }

    public function lastInsertId($sequence = null)
    {
        return $this->pdo->lastInsertId($sequence);
    }

    public function table($tables)
    {
        return $this->query()->from($tables);
    }

    public function query()
    {
        return new QueryBuilder($this, $this->getQueryAdapter());
    }

    public function subQuery(QueryBuilder $query, $alias = '')
    {
        return $this->query()->subQuery($query, $alias);
    }

    public function raw($value, array $bindings = [])
    {
        return new Raw($value, $bindings);
    }

    public function getPdo()
    {
        return $this->pdo;
    }

    public function setPdo(PDO $pdo)
    {
        $this->pdo = $pdo;

        return $this;
    }

    public function getPdoConstantType($var)
    {
        if (is_int($var) || is_bool($var)) {
            return PDO::PARAM_INT;
        }

        return PDO::PARAM_STR;
    }

    public function getQueryAdapter()
    {
        return $this->queryAdapter;
    }

    public function setQueryAdapter(Adapter $queryAdapter)
    {
        $this->queryAdapter = $queryAdapter;

        return $this;
    }

    public function getFetchMode()
    {
        return $this->fetchMode;
    }

    public function setFetchMode($fetchMode)
    {
        $this->fetchMode = func_get_args();

        return $this;
    }

    public function getDriverName()
    {
        return $this->pdo->getAttribute(PDO::ATTR_DRIVER_NAME);
    }

    public function close()
    {
        $this->pdo = null;
    }

    public function __destruct()
    {
        $this->close();
    }
}