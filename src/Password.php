<?php

namespace Shoyu;

/*
 * Clase encargada de la creación y verificación de contraseñas con hasheado
 * fuerte usando las funciones password_* nativas de PHP.
 */

class Password
{
    /**
     * Crea un hash fuerte de una contraseña dada.
     *
     * @param string $password La contraseña en texto plano.
     * @return string|false String de 60 caracteres el cual sería la contraseña
     * hasheada, false en caso de error.
     */
    public static function hash($password)
    {
        return password_hash(
            base64_encode(
                hash('sha512', $password, true)
            ),
            PASSWORD_BCRYPT
        );
    }

    /**
     * Comprueba que la contraseña dada coincide con la contraseña hasheada.
     *
     * @param string $password La contraseña en texto plano
     * @param string $hashedPassowrd La contraseña previamente hasheada con el
     * método ::hash de ésta clase.
     * @return bool True si la contraseña y el hash coinciden, false de lo
     * contrario
     */
    public static function verify($password, $hashedPassowrd)
    {
        return password_verify(
            base64_encode(
                hash('sha512', $password, true)
            ),
            $hashedPassowrd
        );
    }
}