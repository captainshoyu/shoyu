<?php

namespace Shoyu;

use Shoyu\Cookie;

/*
 * Clase que se encarga de manejar sesiones un poco más seguras.
 */
class Session
{
    protected $name;
    protected $sessionId;
    protected $cookieParams = [
        // Si usamos https ponerlo en true.
        'secure' => false,
        // Para que Javascript no acceda al id de la sesión.
        'httpOnly' => true
    ];

    /**
     * Inicia una sesión usando la función nativa de PHP.
     *
     * @return string $sessionId
     */
    public function start()
    {
        if (session_id() !== '') {
            return $this->sessionId;
        }

        $this->initSettings();

        if (! session_start()) {
            throw new \ErrorException('Failed to start the session');
        }

        if (! $this->has('__flashes')) {
            $this->put('__flashes', []);
        }

        return $this->sessionId = session_id();
    }

    /**
     * Inicia las configuraciones para la sesión.
     */
    protected function initSettings()
    {
        // Obligar a las sesiones a sólo usar cookies.
        if (ini_set('session.use_only_cookies', 1) === false) {
            throw new \ErrorException(
                'Could not initiate a safe session'
            );
        }

        // Especificar el archivo de donde serán leidos los bytes para el id.
        // Si estamos en un servidor Linux se usará urandom.
        if (PHP_OS === 'Linux') {
            ini_set('session.entropy_file', '/dev/urandom');
        }

        $this->initCookieParams();

        ini_set('session.entropy_length', 32);
        ini_set('session.hash_function', 'sha256');
        ini_set('session.use_trans_sid', 0);

        if ($this->name) {
            session_name($this->name);
        }
    }

    /**
     * Regenerar la id de la sesión
     *
     * @param bool $deleteOldSession
     */
    public function regenerate($deleteOldSession = false)
    {
        session_regenerate_id($deleteOldSession);
    }

    /**
     * Da un nombre a la sesión
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Obtiene el nombre de la sesión
     *
     * @return string Nombre de la sesión.
     */
    public function getName()
    {
        if ($this->name) {
            return $this->name;
        }

        return session_name();
    }

    /**
     * Establece un valor a la variable de sesión.
     *
     * @return bool False si no se pudo setear el valor, true si se pudo.
     */
    public function put($key, $value)
    {
        $_SESSION[$key] = $value;

        return $this->get($key) === $value;
    }

    /**
     * Obtiene un valor de la variable de sesión.
     *
     * @return null|mixed
     */
    public function get($key)
    {
        return $_SESSION[$key] ?? null;
    }

    /**
     * Elimina un item de $_SESSION y devuelve su valor.
     *
     * @param string $key La clave que se encuentra en la sesión.
     * @return null|mixed $value
     */
    public function pull($key)
    {
        $value = $this->get($key);

        $this->forget($key);

        return $value;
    }

    /**
     * Verfifica que la sesión tenga una clave dada.
     *
     * @return bool
     */
    public static function has($key)
    {
        return array_key_exists($key, $_SESSION);
    }

    /**
     * Elimina un item de $_SESSION.
     * @param string $key
     */
    public function forget($key)
    {
        unset($_SESSION[$key]);
    }

    /**
     * Siempre llamar a ::start antes de ::destroy.
     */
    public function destroy()
    {
        // Vaciar la variable especial $_SESSION.
        $_SESSION = [];
        $cookieParams = session_get_cookie_params();

        // Borrar la cookie de la sesión actual.
        Cookie::forget(
            $this->getName(),
            $cookieParams['path'],
            $cookieParams['domain']
        );

        session_destroy();
    }

    /**
     * Iniciliza los parámetros de la cookie de sesión.
     */
    public function initCookieParams()
    {
        // Obtener los parámetros de las cookies actuales.
        $cookieParams = session_get_cookie_params();

        session_set_cookie_params(
            $cookieParams['lifetime'],
            $cookieParams['path'],
            $cookieParams['domain'],
            $this->cookieParams['secure'],
            $this->cookieParams['httpOnly']
        );
    }

    /**
     * Establece si la cookie debe ser enviada sólo sobre HTTPS.
     *
     * @param bool $secure
     */
    public function cookieSecure(bool $secure)
    {
        $this->cookieParams['secure'] = $secure;
    }

    /**
     * Establece si la cookie de sesión estará disponible para Javascript.
     *
     * @param bool $httpOnly
     */
    public function cookieHTTPOnly(bool $httpOnly)
    {
        $this->cookieParams['httpOnly'] = $httpOnly;
    }

    /**
     * Crea mensajes de sesión.
     *
     * @param string $message El mensaje
     * @param string $level El nivel del mensaje. Ej: warning, error, success...
     */
    public function flash($message, $level = 'info')
    {
        $flashes = $this->get('__flashes');
        $flashes[] = ['message' => $message, 'level' => $level];

        $this->put('__flashes', $flashes);
    }

    /**
     * Devuelve todos los mensajes que fueron flasheados.
     * En caso de que se pase un $level como argumento, se retornará un array conteniendo
     *
     * @return array
     */
    public function getMessages($level = null)
    {
        $flashes = $this->pull('__flashes');
        $messages = [];

        if (! $level) {
            return $flashes;
        }

        foreach ($flashes as $value) {
            if ($value['level'] === $level) {
                $messages[] = $value['message'];
            }
        }

        return $messages;
    }

    /**
     * Indica si hay por lo menos un mensaje flasheado.
     */
    public function hasMessages()
    {
        return count($this->get('__flashes')) > 0;
    }
}