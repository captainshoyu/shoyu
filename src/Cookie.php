<?php

namespace Shoyu;

/**
 * Clase que se encarga de manejar cookies.
 */

class Cookie
{
    /**
     * El dominio por default.
     *
     * @var string $domain
     */
    protected static $domain = null;

    /**
     * El path por default.
     *
     * @var string $path
     */
    protected static $path = '/';

    /**
     * Crea una cookie
     *
     * @param array $props Las propiedades de la cookie.
     * @return bool
     **/
    public static function make($props = [])
    {
        if (
            ! array_key_exists('name', $props) ||
            ! array_key_exists('value', $props)
        ) {
            throw new Exception('make() needs "name" and "value" properties');
        }

        $name = $props['name'];
        $value = $props['value'];
        $path = isset($props['path']) ? $props['path'] : self::$path;
        $domain = isset($props['domain']) ? $props['domain'] : self::$domain;
        $secure = isset($props['secure']) ? $props['secure'] : false;
        $httpOnly = isset($props['httpOnly']) ? $props['httpOnly']: false;

        // Determinar el tiempo de vida de la cookie.
        if (array_key_exists('maxAge', $props)) {
            $lifeTime = $props['maxAge'];

            // Guardarla "para siempre" (5 años en realidad).
            if ($lifeTime == 'forever') {
                $maxAge = time() + 157680000;
            } else {
                $maxAge = time() + $lifeTime;
            }
        } else {
            /* Si es 0, solo durará hasta que el usuario cierre su browser,
             o sea, será una cookie de sesión. */
            $maxAge = 0;
        }

        return setcookie($name, $value, $maxAge, $path,
                         $domain, $secure, $httpOnly);
    }

    /**
     * Hace expirar una cookie.
     *
     * @param string $name
     * @param string $pathinfo
     * @param string $domain
     * @return bool
     */
    public static function forget($name, $path = '', $domain = '')
    {
        // Borrar la cookie alamacenada en $_COOKIE.
        unset($_COOKIE[$name]);

        // Hacer que la cookie expire.
        return self::make([
            'name' => $name,
            'value' => null,
            'maxAge' => -2628000,
            'domain' => $domain,
            'path' => $path
        ]);
    }

    /**
     * Obtiene el valor de una cookie dada.
     * @param string $name
     * @return mixed|null
     */
    public static function get($name)
    {
        return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
    }

    /**
     * Comprueba si existe o no una cookie dada.
     * @param string $name
     * @return bool
     */
    public static function exists($name)
    {
        return array_key_exists($name, $_COOKIE);
    }
}