<?php

namespace Shoyu\HTTP;

class Request
{
    /**
     * Retorna el camino base donde se ejecuta el script principal.
     * Si nuestra aplicación se encuentra en un subdirectorio, como por ejemplo
     * http://example.com/my-app entonces el método devolverá /my-app.
     *
     * @return string
     */
    public static function getBasePath()
    {
        $basePath = dirname($_SERVER['SCRIPT_NAME']);

        if ($basePath == '/') {
            return '';
        } elseif (strlen($basePath) > 1) {
            return $basePath;
        }
    }

    /**
     * Retorna nombre y número de revisión del protocolo de información.
     * Ej: "HTTP/1.1"
     *
     * @return string
     */
    public static function getProtocolInfo()
    {
        return $_SERVER['SERVER_PROTOCOL'];
    }

    /**
     * Retorna el protocolo.
     * Ej: "HTTP/1.1"
     *
     * @return string
     */
    public static function getProtocol()
    {
        $https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : '';

        return ! empty($https) && $https !== 'off' ? 'https' : 'http';
    }

    /**
     * Retorna el nombre del host del servidor.
     *
     * @return string
     */
    public static function hostname()
    {
        return $_SERVER['SERVER_NAME'];
    }

    /**
     * Retorna el puerto usado por el servidor.
     *
     * @return string
     */
    public static function getPort()
    {
        return $_SERVER['SERVER_PORT'];
    }

    /**
     * Retorna el la dirección base del sitio en el que se ejecuta el script
     * principal.
     * Ej: http://www.my-site.com
     *
     * @return string
     */
    public static function getSiteUrl()
    {
        $port = self::getPort();

        return self::getProtocol()
               .'://'
               .self::hostname()
               .(($port == '80') ? '' : (':' . $port));
    }

    /**
     * Devuelve la URL absoluta de la página solicitada por un cliente
     *
     * @return string
     */
    public static function getAbsoluteUrl()
    {
        $uri = urldecode($_SERVER['REQUEST_URI']);

        return self::getSiteUrl() . $uri;
    }

    /**
     * Retorna la cadena de consultas de la URL de la petición actual.
     *
     * @return string
     */
    public static function queryString()
    {
        $uri = parse_url($_SERVER['REQUEST_URI']);

        return isset($uri['query']) ? $uri['query'] : '';
    }

    /**
     * Analiza una cadena de consultas y retorna un array asociativo
     * con las claves y valores de dicha cadena.
     * Ej: "a=10&b=11" -> ["a" => "10", "b" => "11"]
     *
     * @param string $str
     * @return array
     */
    public static function parseQueryString($str)
    {
        $args = [];
        parse_str($str, $arr);

        return $arr;
    }

    /**
     * Retorna las claves y valores parseados de la cadena de consultas
     * en la URL de la petición actual. En caso de que se provea una clave
     * retornará el valor asociado a dicha clave.
     *
     * @param string $key
     * @return mixed
     */
    public static function args($key = '')
    {
        $queryString = self::queryString();

        return self::getRequestData($queryString, $key);
    }

    /**
     * Retorna los datos provenientes de un formulario. Si se provee una clave,
     * devolverá el valor asociado a dicha clave.
     *
     * @param string $key
     * @return mixed
     */
    public static function form($key = '')
    {
        $input = file_get_contents('php://input');

        return self::getRequestData($input, $key);
    }

    /**
     * Helper para ::args() y ::form()
     *
     * @param string $input
     * @param string $key
     * @return mixed
     */
    protected static function getRequestData($input, $key = '')
    {
        $decoded = urldecode($input);
        $data = self::parseQueryString($decoded);

        if (! empty($key)) {
            return isset($data[$key]) ? $data[$key] : null;
        }

        return $data;
    }
}