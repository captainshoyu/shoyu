<?php

namespace Shoyu\HTTP;

class Response
{
    // Una manera un poco más elegante de hacer redirecciones.
    public static function redirect($url, $statusCode = 303)
    {
       header('Location: ' . $url, true, $statusCode);
       exit();
    }

    // Devuelve un string JSON como respuesta al cliente.
    public static function json($mixed)
    {
        header('Content-type: application/json');
        return json_encode($mixed);
    }
}