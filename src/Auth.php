<?php

namespace Shoyu;

use Shoyu\Password;
use Shoyu\Session;

/*
 * Clase encargada de la autenticación e inicio de sesión de los usuarios.
 */

class Auth
{
    /**
     * Modelo que representa al usuario
     *
     * @var string $userModel
     */
    private static $userModel;

    /**
      * El usuario autenticado.
      *
      * @var object $user
      */
    private static $user;


    /**
      * El manejador de sessiones.
      *
      * @var object $session
      */
      private static $session;

    /**
     * Inicializa la sesión.
     */

    protected static function initSession()
    {
        if (! self::$session) {
            self::$session = new Session;
        }
    }

    /**
     * Intenta autenticar un usuario.
     * Éste método asume que el modelo del usuario posee los campos:
     * username, email y password.
     *
     * @param array $credenctials Las credenciales del usuario.
     * @return $user|null Objeto $user si el usuario ha sido autenticado, en
     * caso contrario retornará null.
     */
    public static function authenticate($credentials = [])
    {
        // Verificar cuál campo será usado para la consulta en la BD.
        if (isset($credentials['email'])) {
            $field = 'email';
        } elseif (isset($credentials['username'])) {
            $field = 'username';
        }

        // Si $field no fue definido es porque no se recibió un campo adecuado.
        if (!isset($field)) {
            throw new \Exception(
                'authenticate() needs "email" or "username" keys'
            );
        }

        if (!isset($credentials['password'])) {
            throw new \Exception(
                'authenticate() needs "password" key'
            );
        }

        // Obtener modelo del usuario.
        $userModel = self::getUserModel();
        // Valor con el cual se buscará a un usuario en la BD.
        $value = $credentials[$field];
        // Obtenemos la contraseña desde las credenciales.
        $password = $credentials['password'];
        // Obtenemos al usuario de la BD a travéz del campo y el valor obtenido.
        $user = $userModel::get($field, $value);

        // Si la consulta devolvió un objeto entonces hubo coincidencias.
        if ($user) {
            // Si las contraseñas coinciden retornamos el objeto del usuario.
            if (Password::verify($password, $user->password)) {
                return $user;
            }
        }

        return null;
    }

    /**
     * Guarda los datos del usuario en la sesión actual.
     *
     * @param object $user El objeto que contiene las propiedades de un usuario.
     */
    public static function login($user)
    {
        self::initSession();
        self::$session->put('user_id', $user->id);
        self::$session->put('user_email', $user->email);
        self::$session->put('user_username', $user->username);
        self::$user = $user;
    }

    /**
     * Verifica que el usuario esté autenticado
     *
     * @return bool
     */
    public static function check()
    {
        return ! is_null(self::user());
    }

    /**
     * Obtiene el usuario autenticado.
     *
     * @return null|self::$user
     */
    public static function user()
    {
        // Si ya tenemos un usuario autenticado lo devolvemos inmediatamente
        // para no hacer una consulta a la BD cada vez que llamamos a éste
        // método ;)
        if (! is_null(self::$user)) {
            return self::$user;
        }

        self::initSession();

        // Obtenemos los datos del usuario desde la sesión
        $userId = self::$session->get('user_id');
        $userEmail = self::$session->get('user_email');
        $username = self::$session->get('user_username');

        // Comprobamos que los datos del usuario estén en la sesión.
        if (!$userId || !$userEmail || !$username) {
            return null;
        }

        // Obtenemos el modelo del usuario
        $userModel = self::getUserModel();
        // Buscamos al usuario en la base de datos y tratamos de obtenerlo por
        // medio de su id.
        $user = $userModel::find($userId);

        // Comprobamos que todo esté en orden: que el usuario no sea null,
        // que esté activo, que el email y el username sean iguales a los que
        // están en la sesión actual.
        if (
            is_null($user)
            || !$user->active
            || $user->email !== $userEmail
            || $user->username !== $username
        ) {
            self::logout();
            return null;
        }

        return self::$user = $user;
    }

    /**
     * Borra los datos del usuario de la sesión actual.
     */
    public static function logout()
    {
        self::initSession();
        self::$user = null;
        self::$session->destroy();
    }

    /**
     * Establece el modelo del usuario.
     *
     * @param string $modelName
     */
    public static function setUserModel($modelName)
    {
        if (! class_exists($modelName)) {
            throw new \Exception("Model \"$modelName\" does not exist");
        }

        self::$userModel = $modelName;
    }

     /**
      * Obtiene el modelo del usuario.
      *
      * @return string
      */
    public static function getUserModel()
    {
          return self::$userModel;
    }
}