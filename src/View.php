<?php

namespace Shoyu;

use Twig_Loader_Filesystem;
use Twig_Environment;
use Twig_SimpleFunction;

class View
{
    protected static $viewsPath;

    public static function setViewsPath($path)
    {
        self::$viewsPath = $path;
    }

    public static function render($view, array $data = [])
    {
        if (self::$viewsPath) {
            self::$viewsPath = rtrim(self::$viewsPath, '/') . '/';
        }

        $loader = new Twig_Loader_Filesystem(self::$viewsPath);
        $twig = new Twig_Environment($loader);

        $twig->registerUndefinedFunctionCallback(function($name) {
            if (function_exists($name)) {
                return new Twig_SimpleFunction($name, function() use($name) {
                    return call_user_func_array($name, func_get_args());
                });
            }
            return false;
        });

        return $twig->render($view, $data);
    }
}