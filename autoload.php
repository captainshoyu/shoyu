<?php
//*
spl_autoload_register(function($class) {
    $prefix = strstr($class, '\\', true);

    if ($prefix != 'Shoyu') {
        return;
    }

    $path = str_replace('\\', '/', $class);
    $file =  dirname(__FILE__) . '/src' . strstr($path, '/') . '.php';

    if (is_readable($file)) {
        require_once $file;
    }
});
//*/