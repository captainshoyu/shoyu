# Shoyu

Colección de chapuzas escritas para hacer el desarrollo de sitios en PHP más llevadero. Sólo para proyectos pequeños.

----------

## Instalación

`composer require captainshoyu/shoyu`

```PHP
// Asegúrate de requerir el archivo autoload de Composer.
require 'vendor/autoload.php';
```

```PHP
// Si por alguna razón no puedes usar composer, puedes incluir el propio autoload.php de la librería.
require 'shoyu/autoload.php';
```

## Database

Todo lo necesario para realizar conexiones a bases de datos y ejecutar consultas utilizando SQL plano o el constructor de consultas (Query Builder). Motores soportados:

 - PostgreSQL
 - SQLite
 - MySQL

### Ejemplo de conexión
```php
$factory = new \Shoyu\Database\ConnectionFactory;
$config = [
    'database' => 'dbname',
    'username' => 'your-user',
    'password' => 'your-password',
    'driver'   => 'mysql',
    'host'     => 'localhost',
    'port'     => 5432
    'charset'  => 'utf8'
];

// El segundo argumento de make() nos creará un alias
$conn = $factory->make($config, 'DB')->connect();

// Realizar una consulta
$conn->fetchAll('select * from my_table');

// Realizar una consulta usando el alias creado anteriormente
DB::fetchAll('select * from my_table');
```

### Query shortcuts

```PHP
$firstRow = $conn->fetch('SELECT * FROM users WHERE username = ?', ['captainshoyu']);

$allRows = $conn->fetchAll('SELECT * FROM users WHERE state = ?', ['active']);

$firstColumnFirstRow = $conn->fetchColumn('SELECT COUNT(*) FROM users WHERE state = ?', ['active']);

$affectedRows = $conn->rowCount('UPDATE users SET full_name = ? WHERE id = ?', ['Captain Shoyu', 29]);
```

### Query Builder

#### Obtener todos los registros de una tabla

```PHP
$rows = $conn->table('users')->get();
```

#### Obtener solo el primer registro

```PHP
$row = $conn->table('users')->first();
```

#### Buscar un solo registro por su ID

```PHP
$row = $conn->table('users')->find(6);
```
La consulta anterior supone que la clave primaria de tu tabla se llama `'id'` y que quieres obtener todas las columnas. Si quieres obtener sólo algunas puedes especificarlas en un array:

```PHP
$row = $conn->table('users')->find(6, ['id', 'name', 'email']);
```

#### Buscar un solo registro a través de un campo específico

```PHP
$row = $conn->table('users')->findBy('email', 'shoyu@example.com');

// Especificando las columnas
$row = $conn->table('users')->findBy('email', 'shoyu@example.com', ['id', 'name', 'email']);
```

#### Seleccionar columnas

```PHP
$allRows = $conn->table('users')->select('name', 'email')->get();

// También se puede pasar un array como argumento a select()
$columns = ['id', 'name', 'email', 'status'];
$allRows = $conn->table('users')->select($columns)->get();
```

#### Limit y Offset

```PHP
$conn->table('users')->limit(10)->offset(100);
```

#### Where

```PHP
$conn->table('user')
    ->where('username', '=', 'shoyu')
    ->whereNotIn('age', [10, 20, 30])
    ->orWhere('type', '=', 'admin')
    ->orWhereNot('name', 'LIKE', '%Smith%')
    ->get();
```

La anterior consulta se traduciría a la siguiente sentencia SQL:

    SELECT *
    FROM `user`
    WHERE
        `username` = ? AND
        `age` NOT IN (?, ?, ?) OR
        `type` = ? OR
        NOT `name` LIKE ?

Parámetros de la consulta:
```PHP
["shoyu", 10, 20, 30, "admin", "%Smith%"]
```

#### Where anidados

```PHP
$conn->table('users')
    ->where('age', '>', 10)
    ->orWhere(function ($subWhere) {
        $subWhere->where('surname', '=', 'Meltrozo')
                 ->where('age', '>', 20);
    })
    ->get();
```

    SELECT * FROM `users` WHERE `age` > ? OR (`surname` = ? AND `age` > ?)
    [10, "Meltrozo", 20]

#### Group By y Having

```PHP
$users = DB::table('users')
            ->groupBy('account_id')
            ->having('account_id', '>', 100)
            ->get();
```

#### HavingRaw

```PHP
$users = $conn->table('orders')
                ->select('department', $conn->raw('SUM(price) as total_sales'))
                ->groupBy('department')
                ->havingRaw('SUM(price) > 2500')
                ->get();
```

#### Order by

```PHP
$users = $conn->table('users')
              ->where('type', '<>', 'admin')
              ->orderBy('surname', 'asc')
              ->get();

// También se puede hacer de la siguiente manera
$users = $conn->table('users')
                ->orderBy(['id' => 'asc', 'surname' => 'desc'])
                ->get();

// Lo anterior sería lo mismo que:
// SELECT * FROM `users` ORDER BY `id` asc, `surname` desc
```

#### Joins

```PHP
// INNER JOIN
$users = $conn->table('users')
              ->join('contacts', 'users.id', '=', 'contacts.user_id')
              ->join('orders', 'users.id', '=', 'orders.user_id')
              ->select('users.*', 'contacts.phone', 'orders.price')
              ->get();

// LEFT JOIN
$users = $conn->table('users')
            ->leftJoin('posts', 'users.id', '=', 'posts.user_id')
            ->get();

// JOINs avanzados
$conn->table('users')
        ->join('contacts', function ($join) {
            $join->on('users.id', '=', 'contacts.user_id')
                 ->where('contacts.user_id', '>', 5);
        })
        ->get();
```

El ejemplo de JOIN avanzado visto arriba se traduciría a la siguiente sentencia SQL: 

    SELECT *
    FROM `users`
    INNER JOIN `contacts`
        ON `users`.`id` = `contacts`.`user_id` AND
            `contacts`.`user_id` > ?

#### Subconsultas

```PHP
$query = $conn->table('category')
                ->select('id')
                ->where('name', '=', 'Notebook')
                ->limit(1);
$conn->table('product')
     ->where('category_id', '=', $conn->subQuery($query))
     ->get();
```

    SELECT *
    FROM `product`
    WHERE `category_id` = (
        SELECT `id` FROM `category` WHERE `name` = ? LIMIT 1
    )

#### Aggregates

```PHP
$counted = $conn->table('users')->where('status', 'active')->count();

$minAge = $conn->table('users')->min('age');

$maxAge = $conn->table('users')->max('age');

$average = $conn->table('users')->avg('age');

$sum = $conn->table('users')->sum('age');
```

#### Insert

```PHP
$data = [
    'name' => 'shoyu',
    'email' => 'shoyu@sushiwithshoyu.net',
    'status' => 'active',
    'is_admin' => false,
];

$conn->table('users')->insert($data);

// Obtener el id del registro insertado
$id = $conn->table('users')->insertGetId($data);
```

#### Update

```PHP
$conn->table('users')
     ->where('id', 29)
     ->update([
         'is_admin' => true,
         'email' => 'shoyu@sukiyakiwithshoyu.com'
     ]);
```

#### Increment y Decrement

El constructor de consultas provee métodos que nos permiten incrementar o decrementar el valor de una determinada columna, evitándonos tener que escribir las tediosas sentencias `update`  correspondientes.

```PHP
// Incrementa el valor de 1 en 1 (SET votes = votes + 1)
$conn->table('users')->increment('votes');

// Incrementa el valor de 5 en 5 (SET votes = votes + 5)
$conn->table('users')->increment('votes', 5);

// Decrementa el valor de 1 en 1 (SET votes = votes - 1)
$conn->table('users')->decrement('votes');

// Decrementa el valor de 5 en 5 (SET votes = votes - 5)
$conn->table('users')->decrement('votes', 5);
```

#### Delete
```PHP
$conn->table('users')->delete();

// Borrar todos los usuarios con menos de 100 votos
$conn->table('users')->where('votes', '<', 100)->delete();
```

#### Expresiones "crudas" (Raw Expresions)

Hay ocasiones en donde necesitamos usar expresiones SQL textuales, para ello nos podemos valer del método `$conn->raw` que inyecta una consulta o parte de ella como string, ojo, hay que tener cuidado para evitar una inyección SQL en nuestras consultas. 

```PHP
$users = $conn->table('users')
                ->select($conn->raw('count(*) as user_count, status'))
                ->where('status', '<>', 'blocked')
                ->groupBy('status')
                ->get();
```

#### Obtener SQL y los parámetros a ser utilizados

```PHP
$query = $conn->table('users')
                ->select('id', 'name', 'email')
                ->where('status', '=', 'active');

$query->toSql();
// -> SELECT `id`, `name`, `email` FROM `users` WHERE `status` = ?

$query->getBindings(); // -> ['active']
```

#### Obtener la instancia de PDO

```PHP
$conn->getPdo();
```

## HTTP

### Request

#### Obtener el camino base donde se está ejecutando el script principal

```PHP
// Ejemplo: http://example.com/home
Shoyu\HTTP\Request::getBasePath(); // -> "/home"
```

#### Obtener el nombre y número de revisión del protocolo de información

```PHP
Shoyu\HTTP\Request::getProtocolInfo(); // -> "HTTP/1.1"
```

#### Obtener el protocolo con el cual se está accediendo a nuestro sitio

```PHP
Shoyu\HTTP\Request::getProtocol(); // -> "http"
```

#### Obtener el nombre del host del servidor

```PHP
Shoyu\HTTP\Request::getHostname();
```

#### Obtener el puerto usado por el servidor

```PHP
Shoyu\HTTP\Request::getPort();
```

#### Obtener la URL del sitio

```PHP
// URL: "http://example.com/home"
Shoyu\HTTP\Request::getSiteURL(); // -> "http://example.com"
```

#### Obtener la URL completa de la petición actual

```PHP
Shoyu\HTTP\Request::getAbsoluteURL();
// -> "http://example.com/products/umbrellas/?order=price&dir=desc"
```

#### Obtener la cadena de consultas (Query String) de la URL de la petición actual

```PHP
Shoyu\HTTP\Request::queryString(); // "category_id=10&order=asc"
```

#### Parsear una cadena de consultas
```PHP
Shoyu\HTTP\Request::parseQueryString('category_id=10&order=asc');
// -> ["category_id" => "10", "order" => "asc"]
```

#### Obtener el valor de una clave de la cadena de consultas

`Request::args` nos permite acceder a las claves y valores de una query string.

```PHP
// Ejemplo de URL: http://exmaple.com/?a=10&b=11
// Si quisieramos acceder a todas las claves y valores de la query string
Shoyu\HTTP\Request::args(); // -> ["a" => "10", "b" => "11"]

// Obtener un valor mediante una clave específica
Shoyu\HTTP\Request::args('a'); // -> 10
```

#### Obtener las claves y valores de un formulario

En ocasiones deseamos acceder a los valores enviados por los usuarios a través de formularios web, generalmente por el método HTTP `POST`. `Request::form` nos da acceso a todos esos datos.

```HTML
<h1>Register product</h1>
<form action="/product/register" method="POST">
    <p><label>Name:</label> <input type="text" name="name"></p>
    <p><label>Description:</label> <input type="text" name="desc"></p>
    <p><label>Price:</label> <input type="number" name="price"></p>
    <p><input type="submit" value="Create"></p>
</form>
```

```PHP
// Obtener todos los datos de un formulario anterior
Shoyu\HTTP\Request::form();
// -> ["name" => "Sombrilla", "desc" => "Una linda sombrilla", "price" => "20.50"]

// Obtener un valor por medio de una clave dada
Shoyu\HTTP\Request::form('name'); // -> "Sombrilla"
```

### Response

#### Redirecciones

```PHP
Shoyu\HTTP\Response::redirect('http://example.com/home');
```

#### JSON

```PHP
echo Shoyu\HTTP\Response::json([
    'status' => 'success',
    'response' => 'Producto creado con éxito'
]);
```